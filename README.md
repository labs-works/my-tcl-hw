# Home Work

[![Build Status](https://gitlab.com/labs-works/my-tcl-hw/badges/master/build.svg)](https://gitlab.com/labs-works/my-tcl-hw/commits/master) [![Coverage Report](https://gitlab.com/labs-works/my-tcl-hw/badges/master/coverage.svg)](https://gitlab.com/labs-works/my-tcl-hw/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/labs-works/my-tcl-hw)](https://goreportcard.com/report/gitlab.com/labs-works/my-tcl-hw) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

## Activity guidance
1. [x] Create / Clone an existing project on Github - please choose an existing open source project which doesn’t have a Docker distro already, alternatively improve an existing one (and explain why in your project's README.md)
2. [x] Containerize your service
3. [x] Setup or Use an online CI-service (e.g travis-ci)
4. [x] Supply a deploy directory which includes some configuration management tool |
       Deployment tool - which deploys your docker container on a docker enabled host.
5. [x] Compose a susinct README.md which will enable any new commer to follow a step
by step explanation from the ‘git clone’ untile the ‘running sample’

## Implementation details
### How to run this?
Of course the first thing it to see that this HW is working!!!  
How to do this? Pretty simple - you have to push some change into master branch.  
For example, edit this readme, add some lines at the end of file (or in any place), save, commit and push to master.  
This action will trigger the __CI/CD__ pipeline which you can follow and see the results.

### Repo which was cloned
This project is cloned from [Pantomath-io](https://gitlab.com/pantomath-io/demo-tools/tree/master) repo.
Some golang project with dockerized deliverable (artifact).

### Methodology
I've used SAAS __GitLab__ account which allows quick and simple _CI/CD_ pipeline setup without establishing my own fleet of  
servers/runners/workers etc.  
As a target `docker enabled` server, I've created simple Ubuntu server in AWS free tier (with publicly enabled EIP).  

In my _CI/CD_ implementation I've used 2 approaches:
* CI/CD agnostic base: `make`, using __Makefile__ for steps description so it will be able to run on any local machine and  
  easily implemented in other _CI/CD_ tools.
* GitLab CI/CD approach - just to show this tool options and benefits (dependencies, artifacts automatic transfer between stages).

### CI/CD pipeline highlights
The code is simple __golang__ app which summarizes two numbers - hardcoded 40+2 and prints the result to standard output.  
This app was built, containerized and deployed to __"sandbox"__ machine using _GitLab CI/CD_.  
The following steps/stages were implemented
* Code test
* Build (prepare executable)
* Containerize + test
* Deploy

### Diving deep...
More detailed description of _CI/CD_ stages
1. __Code test/verifications:__
  - Code Lint - verify the code style
  - Unit test
  - [Data Race](https://golang.org/doc/articles/race_detector.html)
  - [MemorySanitizer](https://clang.llvm.org/docs/MemorySanitizer.html)
  - [CodeCoverage](https://blog.golang.org/cover)
2. __Build__  
   In case all tests succeeded, build process will begin.
3. __Containerization__  
   This step depends on __Build__, in case it will succeed its artifacts will be automatically transferred to containerization step directly to project context, so it will be used to prepare the _docker image_.  
   Since this is __GoLang__, there is no need in any _Linux_ base image, it can run in a native `FROM scratch` container.  
   See [Dockerfile](deploy/Dockerfile) for reference.  
   One of internal steps here (just to save the execution time) is docker container test in a runtime,  
   which verifies that containerized app runs properly.
4. __Deploy__  
   _GitLab CI/CD_ supports different deployment methods (including K8S native integration!), in this case I used simple SSH based deployment method, which performs `docker run ...` command on destination AWS instance.

Enjoy!

P.S. Any comments are more than welcome.
