#!/bin/sh

echo "Deploying...."
ssh ubuntu@$UBUNTU_SERVER_IP "docker run registry.gitlab.com/$CI_PROJECT_PATH:latest"
ssh ubuntu@$UBUNTU_SERVER_IP "docker images"
echo "Done."
