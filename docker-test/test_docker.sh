#!/bin/sh
res=$(docker run registry.gitlab.com/labs-works/my-tcl-hw:latest)
control=$(expr 40 + 2)
if [ $control -eq $res ]
then
  echo "Test PASSED: control:$control and res:$res"
  exit_code=0
else
  echo "Test FAILED!!!: control:$control and res:$res"
  exit_code=1
fi
exit ${exit_code}
